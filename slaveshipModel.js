/**
 * Created by Luke on 3/30/2016.
 */
var processSlaveShip = function()
{
    for(var slaveCount=0;slaveCount<3;slaveCount++) {

        var slave = currentSlaves[slaveCount];

        var nameText = slave.name;
        var genderText = slave.gender;
        var ageText = slave.age + " Years Old";
        var sellText = "Buy $" + slave.buyPrice;

        if(slave.notForSale) {
            nameText = "Sold";
            genderText = "---";
            ageText = "---";
            sellText = "---";
        }

        slaveShipNameArray[slaveCount].text = nameText;
        slaveShipGenderArray[slaveCount].text = genderText;
        slaveShipAgeArray[slaveCount].text = ageText;
        slaveShipBuyTextArray[slaveCount].text = sellText;
    }
    canvas.renderAll();

}