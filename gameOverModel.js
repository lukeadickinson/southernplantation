/**
 * Created by Luke on 3/26/2016.
 */

var createGameOverScreen = function(money, score, days, workers, slaves, fields)
{
    var cashText = "Cash: $" + money;
    var scoreText = "Score: " + score;
    var daytext = "Days: " + days;
    var workersText = "Workers: " + workers;
    var slavesText = "Slaves: " + slaves;
    var fieldsText = "Fields: " + fields;

    popupEndGameText3.text = cashText;
    popupEndGameText4.text = scoreText;
    popupEndGameText45.text = daytext;
    popupEndGameText5.text = workersText;
    popupEndGameText6.text = slavesText;
    popupEndGameText7.text = fieldsText;
}



