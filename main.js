/**
 * Created by Luke on 3/26/2016.
 */
window.onload = function () {
    initVisuals();
    initFields();
    initPeople();
    giveMouseEvents();
    resetGame();
    updateSelectedFieldLoop();

    //workerLoop();
    //setSlaveShipAvailable(true);
    //beginDay();
    //setPopupShown(false);

}
