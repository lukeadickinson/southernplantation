/**
 * Created by Luke on 3/26/2016.
 */
var initFields = function()
{
    for(var row=0;row<3;row++) {
        for (var col = 0; col < 3; col++) {
            fields[row][col] = new Field(row, col, fieldsVisual[row][col]);
            fields[row][col].updateImage();
        }
    }
}
var buyField = function(row, col)
{
    if(fields[row][col].state != fieldStates.NotOwned)
        return;

    fields[row][col].state = fieldStates.Planting;
    fields[row][col].percentage =0;
    money -= 100;
    updateMoney(money);
    fields[row][col].updateImage();

}

var sellField = function(row, col)
{
    if(fields[row][col].state == fieldStates.NotOwned)
        return;

    fields[row][col].state = fieldStates.NotOwned;
    fields[row][col].percentage =0;
    money += 80;
    updateMoney(money);

    fields[row][col].updateImage();

}

var fieldStates = {
    NotOwned: 0,
    Planting : 1,
    Growing: 2,
    Harvesting: 3
};
function Field (row, col, fieldObj) {

    this.row = row;
    this.col = col;

    this.fieldObj = fieldObj;

    this.percentage =0;
    this.state =fieldStates.NotOwned;

    this.beingWorked = false;
    this.beingWorkedBy = "";

    this.workField = function()
    {
        var workAmount =2;
        var paidAmount =0;
        var payMultiplyer = .5;

        var scoreplus = 5;

        this.percentage+= workAmount;

        if(this.state == fieldStates.Harvesting)
        {
            paidAmount =  parseInt(workAmount * payMultiplyer);
            scoreplus += 5;
        }
        this.updateState();
        this.updateImage();

        addToScore(scoreplus);

        return paidAmount;
    }
    this.updateState = function()
    {
        switch (this.state)
        {
            case fieldStates.NotOwned:
                break;
            case fieldStates.Planting:
                if(this.percentage >= 100)
                {
                    this.state = fieldStates.Growing;
                    this.percentage -= 100;
                }
                break;
            case fieldStates.Growing:
                if(this.percentage >= 100)
                {
                    this.state = fieldStates.Harvesting;
                    this.percentage -= 100;
                }
                break;
            case fieldStates.Harvesting:
                if(this.percentage >= 100)
                {
                    this.state = fieldStates.Planting;
                    this.percentage -= 100;
                }
                break;
        }
    }
    this.getInfo = function() {
        return this.row + ' ' + this.col + ' apple';
    };
    this.getImageName = function()
    {
        switch (this.state)
        {
            case fieldStates.NotOwned:
                return 'notOwned_image';
                break;
            case fieldStates.Planting:
                if(this.percentage < 50)
                {
                    return 'emptyField_image';
                }
                else
                {
                    return 'planting1_image';
                }

                break;
            case fieldStates.Growing:
                if(this.percentage < 50)
                {
                    return 'planting2_image';
                }
                else
                {
                    return 'growing1_image';
                }
                break;
            case fieldStates.Harvesting:
                if(this.percentage < 50)
                {
                    return 'growing2_image';
                }
                else
                {
                    return 'harvest1_image';
                }
                break;
        }
        return 'notOwned_image';
    };
    this.updateImage = function()
    {
        var image = document.getElementById(this.getImageName());
        fieldObj.setElement(image);
        canvas.renderAll();
    };

    this.getTextSelected = function()
    {
        return "Field Selected: " + this.row + ", " + this.col;
    };
    this.getTextType = function()
    {
        var fieldType = "";
        switch (this.state)
        {
            case fieldStates.NotOwned:
                fieldType = "Not Owned";
                break;
            case fieldStates.Planting:
                fieldType = "Planting";
                break;
            case fieldStates.Growing:
                fieldType = "Growing";
                break;
            case fieldStates.Harvesting:
                fieldType = "Harvesting";
                break;
        }
        return fieldType;

    };
    this.getTextPercent = function()
    {
        return this.percentage +"% Complete";
    };
    this.getSellButtonShown = function()
    {
        if(this.state == fieldStates.NotOwned) {
            return false;
        }

        return true;
    };
    this.getBuyButtonShown = function()
    {
        if(this.state == fieldStates.NotOwned) {
            return true;
        }

        return false;
    };
}