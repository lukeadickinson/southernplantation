/**
 * Created by Luke on 3/26/2016.
 */

var setSlaveShipAvailable = function(isAvailable)
{
    if(isAvailable) {
        slaveShipText.text = "Slave ship available!";
        slaveShipBackground.fill = 'rgba(181,230,29,1)';
        makeElementSelectable(slaveShipGroup);
    }
    else {
        slaveShipText.text = "No slave ship today";
        slaveShipBackground.fill = 'rgba(141,202,20,1)';
        makeElementNotSelectable(slaveShipGroup);
    }
    canvas.renderAll();
}

var updateWeather = function(weather)
{
    weatherText.text = "Weather: " +weather;
    canvas.renderAll();
}
var updateMoney = function(money)
{
    moneyText.text = "$"+money;
    canvas.renderAll();
}
var updateDayCount = function(count)
{
    currentDayText.text = "Day count: "+count;
    canvas.renderAll();
}
var addToScore = function(addtion)
{
    score += addtion;
    scoreText.text = "Score: "+score;
    canvas.renderAll();
}
var restAllWorkers = function()
{
    for(var row=0;row<6;row++) {
        people[row].rest();
        //workerEnergyArray[row].text = "energy: " + people[row].percentageOfEnergy + "%";
        people[row].updatePerson();
    }
}
var setFieldSelected = function(row, col, any)
{
    if(any == false)
    {
        fieldStatusSelectedText.text = "Field Selected: None";
        fieldStatusTypeText.text = "";
        fieldStatusPercentageText.text = "";
        setFieldSellButtonVisiable(false);
        setFieldBuyButtonVisiable(false);
        return;
    }

    fieldStatusSelectedText.text = fields[row][col].getTextSelected();
    fieldStatusTypeText.text = fields[row][col].getTextType();
    fieldStatusPercentageText.text = fields[row][col].getTextPercent();

    setFieldSellButtonVisiable(fields[row][col].getSellButtonShown());
    setFieldBuyButtonVisiable(fields[row][col].getBuyButtonShown());

}

var setFieldSellButtonVisiable= function(isShown)
{
    if(isShown) {
        canvas.add(fieldStatusSellGroup);
    }
    else {
        canvas.remove(fieldStatusSellGroup);
    }
    canvas.renderAll();
}
var setFieldBuyButtonVisiable= function(isShown)
{
    if(isShown) {
        canvas.add(fieldStatusBuyGroup);
    }
    else {
        canvas.remove(fieldStatusBuyGroup);
    }
    canvas.renderAll();
}

var setPopupShown= function(isShown)
{
    if(isShown) {
        canvas.add(popupScreenBackground);
        canvas.add(popupBackground);
    }
    else {
        canvas.remove(popupScreenBackground);
        canvas.remove(popupBackground);
        setDailyUpdateShown(false);
        setEndGameShown(false);
    }
    canvas.renderAll();
}
var setSlaveDataShow= function(isShown)
{
    if(isShown) {
        canvas.add(slaveShipPopUpGroup);
        for(var slaveCount=0;slaveCount<3;slaveCount++) {
            canvas.add(slaveShipBuyGroupArray[slaveCount]);

        }
    }
    else {
        canvas.remove(slaveShipPopUpGroup);
        for(var slaveCount=0;slaveCount<3;slaveCount++) {
            canvas.remove(slaveShipBuyGroupArray[slaveCount]);
        }
    }
    canvas.renderAll();
}
var setDailyUpdateShown= function(isShown)
{
    if(isShown) {
        canvas.add(popupDailyUpdateGroup);
    }
    else {
        canvas.remove(popupDailyUpdateGroup);
    }
    canvas.renderAll();
}
var setEndGameShown= function(isShown)
{
    if(isShown) {
        canvas.add(popupEndGameGroup);
    }
    else {
        canvas.remove(popupEndGameGroup);
    }
    canvas.renderAll();
}
var unselectField = function()
{
    fieldSelected = null;
    setFieldBuyButtonVisiable(false);
    setFieldSellButtonVisiable(false);

}
var processBoughtSlaves = function()
{
    while(boughtSlaves.length > 0)
    {
        var slave = boughtSlaves.pop();
        slave.changePersonType();
    }
}
var giveMouseEvents = function()
{
    popupScreenBackground.on('selected', function() {
        unselectField();

        setPopupShown(false);
        setSlaveDataShow(false);
        processBoughtSlaves();
        if(dayEnded)
        {
            dayEnded = false;
            //workerLoop();
        }
    });

    hireWorkerGroup.on('selected', function() {

        buyWorker();
        canvas.deactivateAll().renderAll();
    });

    workerSellGroupArray[0].on('selected', function() {
        sellWorker(0);
    });
    workerSellGroupArray[1].on('selected', function() {
        sellWorker(1);
    });
    workerSellGroupArray[2].on('selected', function() {
        sellWorker(2);
    });
    workerSellGroupArray[3].on('selected', function() {
        sellWorker(3);
    });
    workerSellGroupArray[4].on('selected', function() {
        sellWorker(4);
    });
    workerSellGroupArray[5].on('selected', function() {
        sellWorker(5);
    });

    workerWorkGroupArray[0].on('selected', function() {
        workWorker(0);
    });
    workerWorkGroupArray[1].on('selected', function() {
        workWorker(1);
    });
    workerWorkGroupArray[2].on('selected', function() {
        workWorker(2);
    });
    workerWorkGroupArray[3].on('selected', function() {
        workWorker(3);
    });
    workerWorkGroupArray[4].on('selected', function() {
        workWorker(4);
    });
    workerWorkGroupArray[5].on('selected', function() {
        workWorker(5);
    });

    slaveShipGroup.on('selected', function() {
        unselectField();

        processSlaveShip();
        setPopupShown(true);
        setSlaveDataShow(true);

    });
    slaveShipBuyGroupArray[0].on('selected', function() {
        buySlave(0);
        processSlaveShip();
    });
    slaveShipBuyGroupArray[1].on('selected', function() {
        buySlave(1);
        processSlaveShip();
    });
    slaveShipBuyGroupArray[2].on('selected', function() {
        buySlave(2);
        processSlaveShip();
    });
    endTurnGroup.on('selected', function() {
        dayEnded =true;
        unselectField();

        beginDay();

    });

    fieldStatusSellGroup.on('selected', function() {

        sellField(fieldSelected.row,fieldSelected.col);
    });


    fieldStatusBuyGroup.on('selected', function() {

        buyField(fieldSelected.row,fieldSelected.col);
    });

    fieldsVisual[0][0].on('selected', function() {
        fieldSelected = fields[0][0];
    });
    fieldsVisual[1][0].on('selected', function() {
        fieldSelected = fields[1][0];
    });
    fieldsVisual[2][0].on('selected', function() {
        fieldSelected = fields[2][0];
    });
    fieldsVisual[0][1].on('selected', function() {
        fieldSelected = fields[0][1];
    });
    fieldsVisual[1][1].on('selected', function() {
        fieldSelected = fields[1][1];
    });
    fieldsVisual[2][1].on('selected', function() {
        fieldSelected = fields[2][1];
    });
    fieldsVisual[0][2].on('selected', function() {
        fieldSelected = fields[0][2];
    });
    fieldsVisual[1][2].on('selected', function() {
        fieldSelected = fields[1][2];
    });
    fieldsVisual[2][2].on('selected', function() {
        fieldSelected = fields[2][2];
    });

}

var updateSelectedFieldLoop = function() {
    if(fieldSelected == null){
        setFieldSelected(0, 0, false);

    }
    else {
        setFieldSelected(fieldSelected.row, fieldSelected.col, true);
    }
    setTimeout(updateSelectedFieldLoop,200);

}
