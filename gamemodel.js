/**
 * Created by Luke on 3/26/2016.
 */

var currentWeather = weatherStates.sunny;
var currentDayCount = "1";
var score = 0;

var money = 1000;
var moneyPaidToday =0;
var fieldSelected = null;

var fields = [[],[],[]];
var dayEnded = false;
var people =[];

var currentSlaves=[];

var boughtSlaves =[];

var getRandom = function (min, max)
{
    return (Math.random() * (max - min)) + min;
}
var generateCurrentSlaves = function () {
    for (var col = 0; col < 3; col++) {
        var slave = createSlave();
        currentSlaves[col] = slave;
    }
}

var generateSlaveShipAvailability = function () {
    var available = parseInt(getRandom(1,3)) != 1;
    setSlaveShipAvailable(available);
};
var resetGame= function () {
    initFields();
    buyField(0,0);
    buyField(0,1);
    initPeople();
    generateCurrentSlaves();
    restAllWorkers();
    currentWeather = weatherStates.sunny;
    currentDayCount = "1";
    score = 0;
    addToScore(0);
    money = 1000;

    fieldSelected = null;
    setSlaveShipAvailable(true);
    updateDailyElements();

    //todo fill in all starting data
};

var beginDay = function()
{
    currentDayCount++;
    currentWeather = generateCurrentWeather();
    generateSlaveShipAvailability();
    money = money + calculateDailyLivingTax(countWorkersOwned(),countSlavesOwned());
    generateCurrentSlaves();
    restAllWorkers();
    updateDailyElements();
    setPopupShown(true);
    addToScore(100);

    if(money < 0)
    {
        createGameOverScreen(money, score, currentDayCount, 2, 5, 4);
        setEndGameShown(true);
        resetGame();
    }
    else {
        processDailyStatus(countWorkersOwned(),countSlavesOwned());
        setDailyUpdateShown(true);
        moneyPaidToday = 0;
        addToScore(1500);
    }
};
var countSlavesOwned = function()
{
    var count =0;
    for(var row=0;row<6;row++) {
        if(people[row].personType == personTypes.slave)
        {
            count++;
        }
    }
    return count;
}
var countWorkersOwned = function()
{
    var count =0;
    for(var row=0;row<6;row++) {
        if(people[row].personType == personTypes.worker)
        {
            count++;
        }
    }
    return count;
}

var updateDailyElements = function()
{
    updateDayCount(currentDayCount);
    updateWeather(currentWeather);
    updateMoney(money);
}
var payMoney = function(amountPaid)
{
    money += amountPaid;
    moneyPaidToday += amountPaid;
    updateMoney(money);
}



