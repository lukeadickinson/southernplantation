
    var canvas;

    var fieldsVisual = [[],[],[]];
    var brownfieldBackground;

    var statusBarGroup;
    var statusBarBackground;
    var currentDayText;
    var scoreText;
    var weatherText;

    var moneyTitleText;
    var moneyText;

    var fieldStatusGroup;
    var fieldStatusBackground;
    var fieldStatusSelectedText;
    var fieldStatusTypeText;
    var fieldStatusPercentageText;

    var fieldStatusSellBackground;
    var fieldStatusSellText;
    var fieldStatusSellGroup;

    var fieldStatusBuyBackground;
    var fieldStatusBuyText;
    var fieldStatusBuyGroup;

    var workerGroupArray =[];
    var workerBackgroundArray=[];
    var workerNameTextArray=[];
    var workerImageArray=[];

    var workerAgeGenderArray=[];
    var workerHealthArray=[];
    var workerEnergyArray=[];

    var workerActiveArray=[];
    var workerSellBackgroundArray=[];
    var workerSellTextArray=[];
    var workerSellGroupArray=[];

    var workerWorkBackgroundArray=[];
    var workerWorkTextArray=[];
    var workerWorkGroupArray=[];


    var hireWorkerBackground;
    var hireWorkerText;
    var hireWorkerGroup;

    var slaveShipBackground;
    var slaveShipText;
    var slaveShipGroup;

    var slaveStatusBackground;
    var endTurnBackground;
    var endTurnText;
    var endTurnGroup;

    var popupScreenBackground;
    var popupBackground;

    var popupDailyUpdateTitle;
    var popupDailyUpdateText2;
    var popupDailyUpdateText3;
    var popupDailyUpdateText4;
    var popupDailyUpdateText5;
    var popupDailyUpdateText6;
    var popupDailyUpdateText7;
    var popupDailyUpdateGroup;

    var popupEndGameTitle;
    var popupEndGameText2;
    var popupEndGameText3;
    var popupEndGameText4;
    var popupEndGameText45;
    var popupEndGameText5;
    var popupEndGameText6;
    var popupEndGameText7;
    var popupEndGameText8;

    var popupEndGameGroup;

    //TODO do slaveshipVisuals
    var slaveShipTitleText;
    var slaveShipPopUpGroup;


    var slaveShipGroupArray = [];
    var slaveShipNameArray = [];
    var slaveShipGenderArray = [];
    var slaveShipAgeArray =[];
    var slaveShipBuyBackgroundArray = [];
    var slaveShipBuyTextArray = [];
    var slaveShipBuyGroupArray = [];



    var makeElementSelectable = function(element)
    {
        element.hasControls = false;
        element.lockMovementX = true;
        element.lockMovementY = true;
        element.lockRotation = true;
        element.lockScalingX = true;
        element.lockScalingY = true;
    }
    var makeElementNotSelectable = function(element)
    {
        element.selectable = false;
    }
    var groupObjects = function(obj1, obj2, left, top)
    {
        var group = new fabric.Group([ obj1, obj2 ], {
            left: left,
            top: top
        });

        return group;
    }

    var initVisuals = function () {

        //create elements
        canvas = new fabric.Canvas('c');

        for(var row=0;row<3;row++) {
            for (var col = 0; col < 3; col++) {
                fieldsVisual[row][col] = createField(row, col);
            }
        }
        statusBarBackground = createBarBackground();
        currentDayText = createCurrentDayText();
        scoreText = createScoreText();
        weatherText = createCurrentWeatherText();

        moneyTitleText = createMoneyTitleText();
        moneyText = createMoneyText();

        fieldStatusBackground = createFieldStatusBackground();
        fieldStatusSelectedText = createFieldStatusSelectedText();
        fieldStatusTypeText = createFieldStatusTypeText();
        fieldStatusPercentageText = createFieldStatusPercentageText();

        fieldStatusGroup = new fabric.Group([
            fieldStatusBackground,
            fieldStatusSelectedText,
            fieldStatusTypeText,
            fieldStatusPercentageText], {
        });



        for(var slaveCount=0;slaveCount<3;slaveCount++) {
            slaveShipNameArray[slaveCount] = createSlaveShipThirdText(slaveCount);
            slaveShipGenderArray[slaveCount]  =createSlaveShipFourthText(slaveCount) ;
            slaveShipAgeArray[slaveCount]  = createSlaveShipFifthText(slaveCount);

            slaveShipBuyBackgroundArray[slaveCount]  = createSlaveShipBuyBackground();
            slaveShipBuyTextArray[slaveCount]  = createSlaveShipBuyText();

            slaveShipBuyGroupArray[slaveCount] = groupObjects(
                slaveShipBuyBackgroundArray[slaveCount],
                slaveShipBuyTextArray[slaveCount],
                slaveShipLeft(slaveCount),
                popupTextSeventhTop);

            slaveShipGroupArray[slaveCount] = new fabric.Group([
                slaveShipNameArray[slaveCount],
                slaveShipGenderArray[slaveCount],
                slaveShipAgeArray[slaveCount]
                ],{
            });
        }

        slaveShipTitleText = createSlaveShipTitleText();
        slaveShipPopUpGroup = new fabric.Group([
            slaveShipGroupArray[0],
            slaveShipGroupArray[1],
            slaveShipGroupArray[2],
            slaveShipTitleText], {
        });


        for(var personNumber=0;personNumber<6;personNumber++) {
            //workerBackgroundArray[personNumber] = createWorkerBackground(personNumber);
            workerNameTextArray[personNumber] = createWorkerNameText(personNumber);
            workerImageArray[personNumber] = createWorkerFace(personNumber);

            workerAgeGenderArray[personNumber] = createWorkerAgeGenderText(personNumber);
            workerHealthArray[personNumber] = createWorkerHealthText(personNumber);
            workerActiveArray[personNumber] = createWorkerStatusText(personNumber);

            workerEnergyArray[personNumber] = createWorkerEnergyText(personNumber);

            workerSellBackgroundArray[personNumber] = createWorkerSellBackground();
            workerSellTextArray[personNumber] = createWorkerSellText();
            workerWorkBackgroundArray[personNumber] = createWorkerWorkBackground();
            workerWorkTextArray[personNumber] = createWorkerWorkText();


            workerSellGroupArray[personNumber] = groupObjects(
                workerWorkBackgroundArray[personNumber],
                workerSellTextArray[personNumber],
                personFirstTextLeft - 25,
                person4TextTop + personHeight * personNumber);

            workerWorkGroupArray[personNumber] = groupObjects(
                workerWorkBackgroundArray[personNumber],
                workerWorkTextArray[personNumber],
                person2TextLeft,
                person4TextTop+ personHeight * personNumber);

            workerGroupArray[personNumber] = new fabric.Group([
               // workerBackgroundArray[personNumber],
                workerNameTextArray[personNumber],
                workerImageArray[personNumber],
                workerAgeGenderArray[personNumber],
                workerHealthArray[personNumber],
                workerEnergyArray[personNumber]
            ], {});
        }
        statusBarGroup = new fabric.Group([
            statusBarBackground,
            currentDayText,
            scoreText,
            weatherText,
            moneyTitleText,
            moneyText,
            fieldStatusGroup], {
            left: 0,
            top: 0
        });

        fieldStatusSellBackground =createFieldStatusSellBackground();
        fieldStatusSellText = createFieldStatusSellText();
        fieldStatusSellGroup = groupObjects(
            fieldStatusSellBackground,
            fieldStatusSellText,
            fieldStatusSecondTextLeft,
            barMiddleTextHeight);

        fieldStatusBuyBackground =createFieldStatusBuyBackground();
        fieldStatusBuyText = createFieldStatusBuyText();
        fieldStatusBuyGroup = groupObjects(
            fieldStatusBuyBackground,
            fieldStatusBuyText,
            fieldStatusSecondTextLeft,
            barMiddleTextHeight);

        hireWorkerBackground =createHireWorkerBackground();
        hireWorkerText = createHireWorkerText();
        hireWorkerGroup = groupObjects(
            hireWorkerBackground,
            hireWorkerText,
            sidebarLeft,
            barHeight);

        slaveShipBackground = createSlaveShipBarBackground();
        slaveShipText = createSlaveShipText();
        slaveShipGroup = groupObjects(slaveShipBackground, slaveShipText, sidebarLeft , 0);

        slaveStatusBackground = createSlaveBarBackground();


        endTurnBackground = createDoneBarBackground();
        endTurnText = CreateEndDayText();
        endTurnGroup = groupObjects(endTurnBackground, endTurnText, sidebarLeft, endDayTop);

        brownfieldBackground =createFieldBackground();


        popupScreenBackground = createPopupGrayScreen();
        popupBackground = createPopupBackground();

        popupDailyUpdateTitle = createPopupTitleText();
        popupDailyUpdateText2 = createPopupSecondText();
        popupDailyUpdateText3 = createPopupThirdText();
        popupDailyUpdateText4 = createPopupFourthText();
        popupDailyUpdateText5 = createPopupFifthText();
        popupDailyUpdateText6 = createPopupSixthText();
        popupDailyUpdateText7 = createPopupSeventhText();

        popupDailyUpdateGroup = new fabric.Group([
            popupDailyUpdateTitle,
            popupDailyUpdateText2,
            popupDailyUpdateText3,
            popupDailyUpdateText4,
            popupDailyUpdateText5,
            popupDailyUpdateText6,
            popupDailyUpdateText7], {
        });


        popupEndGameTitle = createEndGameTitleText();
        popupEndGameText2 = createEndGameSecondText();
        popupEndGameText3 = createEndGameThirdText();
        popupEndGameText4 = createEndGameFourthText();
        popupEndGameText45 = createEndGameFourAndHalfText();
        popupEndGameText5 = createEndGameFifthText();
        popupEndGameText6 = createEndGameSixthText();
        popupEndGameText7 = createEndGameSeventhText();
        popupEndGameText8 = createEndGameEighthText();

        popupEndGameGroup = new fabric.Group([
            popupEndGameTitle,
            popupEndGameText2,
            popupEndGameText3,
            popupEndGameText4,
            popupEndGameText45,
            popupEndGameText5,
            popupEndGameText6,
            popupEndGameText7,
            popupEndGameText8], {
        });

        //selectable
        for(var row=0;row<3;row++) {
            for (var col = 0; col < 3; col++) {
                makeElementSelectable(fieldsVisual[row][col])
            }
        }
        for(var personNumber=0;personNumber<6;personNumber++) {
            makeElementNotSelectable(workerGroupArray[personNumber]);
            makeElementSelectable(workerWorkGroupArray[personNumber]);
            makeElementSelectable(workerSellGroupArray[personNumber]);
        }
        for (var col = 0; col < 3; col++) {
            makeElementSelectable(slaveShipBuyGroupArray[col]);
        }
        makeElementNotSelectable(slaveShipPopUpGroup);

        makeElementNotSelectable(statusBarGroup);
        makeElementSelectable(fieldStatusSellGroup);
        makeElementSelectable(fieldStatusBuyGroup);
        makeElementSelectable(hireWorkerGroup);

        makeElementSelectable(slaveShipGroup);
        makeElementNotSelectable(slaveStatusBackground);
        makeElementSelectable(endTurnGroup);
        makeElementNotSelectable(brownfieldBackground);

        makeElementSelectable(popupScreenBackground);
        makeElementSelectable(popupBackground);

        makeElementNotSelectable(popupDailyUpdateGroup);
        makeElementNotSelectable(popupEndGameGroup);

        //add to canvas

        canvas.add(statusBarGroup);

        canvas.add(slaveStatusBackground);
        canvas.add(endTurnGroup);
        canvas.add(hireWorkerGroup);



        canvas.add(brownfieldBackground);
        for(var row=0;row<3;row++) {
            for (var col = 0; col < 3; col++) {
                canvas.add(fieldsVisual[row][col]);
            }
        }

        //workers
        for(var personNumber=0;personNumber<6;personNumber++) {
            canvas.add(workerGroupArray[personNumber]);
            canvas.add(workerWorkGroupArray[personNumber]);
            canvas.add(workerSellGroupArray[personNumber]);
        }
        canvas.add(slaveShipGroup);

    }

    var createField = function(row, col) {
        var imgElement = document.getElementById('emptyField_image');
        var field = new fabric.Image(imgElement,{
            left: spacing + row*(fieldWidth + spacing),
            top: barHeight + spacing+ col*(fieldHeight + spacing),
            width: fieldWidth,
            height: fieldHeight
        });
        field.set({ strokeWidth: 3, stroke: 'rgba(0,0,0,1)' });

        return field;
    }
    var createFieldBackground = function() {
        var imgElement = document.getElementById('fieldbackground_image');

        var fieldbackground = new fabric.Image(imgElement,{
            left: 0,
            top: barHeight,
            width: (fieldWidth +spacing) * 3 + spacing,
            height: (fieldHeight +spacing) * 3 + spacing
        });

        return fieldbackground;
    }

    var createBarBackground = function() {
        var barBackground = new fabric.Rect({
            left: 0,
            top: 0,
            fill: 'rgba(255,170,0,1)',
            width: statusBarWidth - 4,
            height: barHeight - 4
        });
        barBackground.set({ strokeWidth: 4, stroke: 'rgba(255,127,39,.5)' });

        return barBackground;
    }
    var createCurrentDayText = function() {
        var text = new fabric.Text('Day count: 7', {
            fontSize: littleText,
            left: barFirstTextLeft,
            top: barMiddleTextHeight
        });
        return text;
    }
    var createScoreText = function() {
        var text = new fabric.Text('Score: 0000000', {
            fontSize: littleText,
            left: barFirstTextLeft,
            top: barBottomTextHeight
        });
        return text;
    }
    var createCurrentWeatherText = function() {
        var text = new fabric.Text('Sunny', {
            fontSize: littleText,
            left: barFirstTextLeft,
            top: barTopTextHeight
        });
        return text;
    }

    var createMoneyTitleText = function() {
        var text = new fabric.Text('Money:', {
            fontSize: littleText,
            left: barSecondTextLeft,
            top: barTopTextHeight
        });
        return text;
    }
    var createMoneyText = function() {
        var text = new fabric.Text('$10,000', {
            fontSize: bigText,
            left: barSecondTextLeft,
            top: barBigBottomTextHeight
        });
        return text;
    }

    var createWorkerBackground = function(personNumber) {
        var barBackground = new fabric.Rect({
            fill: 'rgba(125,125,125,.00)',
            left: fieldStatusLeft,
            top: personBackgroundTop + personHeight * personNumber,
            width: statusBarWidth -2,
            height: barHeight - 2
        });
        return barBackground;
    }
    var createWorkerNameText = function(personNumber) {
        var text = new fabric.Text('John Akka', {
            fontSize: tinyText,
            left: personFirstTextLeft,
            top: personFirstTextTop + personHeight * personNumber
        });
        return text;
    }
    var createWorkerAgeGenderText = function(personNumber) {
        var text = new fabric.Text('age 25, male', {
            fontSize: tinyText,
            left: person2TextLeft,
            top: personFirstTextTop + personHeight * personNumber
        });
        return text;
    }
    var createWorkerEnergyText = function(personNumber) {
        var text = new fabric.Text('Energy: 100%', {
            fontSize: tinyText,
            left: personFirstTextLeft - 25,
            top: person4TextTop + personHeight * personNumber
        });
        return text;
    }
    var createWorkerHealthText = function(personNumber) {
        var text = new fabric.Text('Health: regular', {
            fontSize: tinyText,
            left: person2TextLeft,
            top: person2TextTop + personHeight * personNumber
        });
        return text;
    }
    var createWorkerStatusText = function(personNumber) {
        var text = new fabric.Text('Status: resting', {
            fontSize: tinyText,
            left: person2TextLeft,
            top: person3TextTop + personHeight * personNumber
        });
        return text;
    }
    var createWorkerSellBackground = function() {
        var barBackground = new fabric.Rect({
            originX: 'center',
            originY: 'center',
            fill: 'rgba(243,217,177,1)',
            width: personFirstTextLeft - 25,
            height: sellButtonHeight - 2
        });
        barBackground.set({ strokeWidth: 2, stroke: 'rgba(0,0,255,.5' });
        return barBackground;
    }
    var createWorkerSellText = function() {
        var text = new fabric.Text('Lay Off', {
            fontSize: tinyText,
            originX: 'center',
            originY: 'center'
        });
        return text;
    }
    var createWorkerWorkBackground = function() {
        var barBackground = new fabric.Rect({
            originX: 'center',
            originY: 'center',
            fill: 'rgba(243,217,177,1)',
            width: sellButtonWidth -2,
            height: sellButtonHeight - 2
        });
        barBackground.set({ strokeWidth: 2, stroke: 'rgba(0,0,255,.5' });
        return barBackground;
    }
    var createWorkerWorkText = function() {
        var text = new fabric.Text('Work Field', {
            fontSize: tinyText,
            originX: 'center',
            originY: 'center'
        });
        return text;
    }
    var createWorkerFace = function(personNumber) {
        var circle = new fabric.Ellipse({
            ry : 20,
            rx  : 17,
            fill: 'brown',
            left: personImageLeft,
            top: personFirstTextTop + personHeight *personNumber
        });
        return circle;
    }

    var createFieldStatusBackground = function() {
        var barBackground = new fabric.Rect({
            fill: 'rgba(125,125,125,.01)',
            left: fieldStatusLeft,
            top: 0,
            width: fieldStatusWidth -4,
            height: barHeight - 4
        });
        barBackground.set({ strokeWidth: 4, stroke: 'rgba(125,125,125,.5)' });
        return barBackground;
    }

    var createFieldStatusSelectedText = function() {
        var text = new fabric.Text('Field Selected: None', {
            fontSize: littleText,
            left: fieldStatusFirstTextLeft,
            top: barTopTextHeight
        });
        return text;
    }
    var createFieldStatusTypeText = function() {
        var text = new fabric.Text('Harvesting', {
            fontSize: littleText,
            left: fieldStatusFirstTextLeft,
            top: barMiddleTextHeight
        });
        return text;
    }
    var createFieldStatusPercentageText = function() {
        var text = new fabric.Text('75% Complete', {
            fontSize: littleText,
            left: fieldStatusFirstTextLeft,
            top: barBottomTextHeight
        });
        return text;
    }

    var createFieldStatusSellBackground = function() {
        var barBackground = new fabric.Rect({
            originX: 'center',
            originY: 'center',
            fill: 'rgba(243,217,177,1)',
            width: sellButtonWidth -2,
            height: sellButtonHeight - 2
        });
            barBackground.set({ strokeWidth: 2, stroke: 'rgba(0,0,255,.5' });
        return barBackground;
    }
    var createFieldStatusSellText = function() {
        var text = new fabric.Text('Sell $80', {
            top:2,
            fontSize: tinyText,
            originX: 'center',
            originY: 'center'
        });
        return text;
    }

    var createFieldStatusBuyBackground = function() {
        var barBackground = new fabric.Rect({
            originX: 'center',
            originY: 'center',
            fill: 'rgba(243,217,177,1)',
            width: sellButtonWidth -2,
            height: sellButtonHeight - 2
        });
        barBackground.set({ strokeWidth: 2, stroke: 'rgba(0,0,255,.5' });
        return barBackground;
    }
    var createFieldStatusBuyText = function() {
        var text = new fabric.Text('Buy $100', {
            top:2,
            fontSize: tinyText,
            originX: 'center',
            originY: 'center'
        });
        return text;
    }

    var createHireWorkerBackground = function() {
        var barBackground = new fabric.Rect({
            originX: 'center',
            originY: 'center',
            fill: 'rgba(243,217,177,1)',
            width: sidebarWidth -2,
            height: sellButtonHeight - 2
        });
        barBackground.set({ strokeWidth: 2, stroke: 'rgba(0,0,255,.5' });
        return barBackground;
    }
    var createHireWorkerText = function() {
        var text = new fabric.Text('Hire Field Hand', {
            top:2,
            fontSize: tinyText,
            originX: 'center',
            originY: 'center'
        });
        return text;
    }
    var createSlaveShipBarBackground = function() {
        var barBackground = new fabric.Rect({
            originX: 'center',
            originY: 'center',
            fill: 'rgba(141,202,20,1)',
            width: sidebarWidth -4,
            height: barHeight - 4
        });
        barBackground.set({ strokeWidth: 4, stroke: 'rgba(34,177,76,.5' });
        return barBackground;
    }
    var createSlaveShipText = function() {
        var text = new fabric.Text('hello world', {
            fontSize: normalText,
            originX: 'center',
            originY: 'center'
        });
        return text;
    }

    var createSlaveBarBackground = function() {
        var barBackground = new fabric.Rect({
            left: (fieldWidth + spacing) * 3 + spacing,
            top: barHeight,
            fill: 'rgba(153,217,234,1)',
            width: canvasWidth - sidebarLeft - 4,
            height: canvasHeight - barHeight - doneHeight - 4
        });
        barBackground.set({strokeWidth: 4, stroke: 'rgba(0,162,232,.5'});

        return barBackground;
    }

    var createDoneBarBackground = function() {
        var barBackground = new fabric.Rect({
            originX: 'center',
            originY: 'center',
            fill: 'rgba(255,100,150,1)',
            width: endDayWidth -4,
            height: doneHeight - 4
        });
        barBackground.set({ strokeWidth: 4, stroke: 'rgba(237,28,36,.5)' });
        return barBackground;

    }
    var CreateEndDayText = function() {
        var text = new fabric.Text('End Day', {
            fontSize: normalText,
            originX: 'center',
            originY: 'center'
        });
        return text;
    }

    var createPopupGrayScreen = function() {
        var barBackground = new fabric.Rect({
            top: 0,
            left: 0,
            fill: 'rgba(0,0,0,.5)',
            width: canvasWidth,
            height: canvasHeight
        });
        return barBackground;
    }
    var createPopupBackground = function() {
        var barBackground = new fabric.Rect({
            top: popupLeft,
            left: popupTop,
            fill: 'rgba(200,200,200,1)',
            width: popupWidth,
            height: popupHeight
        });
        barBackground.set({ strokeWidth: 4, stroke: 'rgba(130,130,130,1)' });

        return barBackground;
    }

    var createSlaveShipTitleText = function() {
        var text = new fabric.Text('Slave Ship', {
            fontSize: bigText,
            left: popupTextMiddleLeft,
            top: popupTextFirstTop
        });
        return text;
    }
    var createSlaveShipThirdText = function(col) {
        var text = new fabric.Text('John Akka', {
            fontSize: normalText,
            left: slaveShipLeft(col),
            top: popupTextThirdTop
        });
        return text;
    }
    var createSlaveShipFourthText = function(col) {
        var text = new fabric.Text('Male', {
            fontSize: normalText,
            left: slaveShipLeft(col),
            top: popupTextFourthTop
        });
        return text;
    }
    var createSlaveShipFifthText = function(col) {
        var text = new fabric.Text('29 Years Old', {
            fontSize: normalText,
            left: slaveShipLeft(col),
            top: popupTextFifthTop
        });
        return text;
    }
    var createSlaveShipBuyBackground = function() {
        var barBackground = new fabric.Rect({
            originX: 'center',
            originY: 'center',
            fill: 'rgba(243,217,177,1)',
            width: sellButtonWidth -2,
            height: sellButtonHeight - 2
        });
        barBackground.set({ strokeWidth: 2, stroke: 'rgba(0,0,255,.5' });
        return barBackground;
    }
    var createSlaveShipBuyText = function() {
        var text = new fabric.Text('Buy $75', {
            fontSize: tinyText,
            originX: 'center',
            originY: 'center'
        });
        return text;
    }

    var createPopupTitleText = function() {
        var text = new fabric.Text('Daily Update', {
            fontSize: bigText,
            left: popupTextMiddleLeft,
            top: popupTextFirstTop
        });
        return text;
    }
    var createPopupSecondText = function() {
        var text = new fabric.Text('Personal daily costs:', {
            fontSize: normalText,
            left: popupTextLeft,
            top: popupTextSecondTop
        });
        return text;
    }
    var createPopupThirdText = function() {
        var text = new fabric.Text('Workers daily wages:', {
            fontSize: normalText,
            left: popupTextLeft,
            top: popupTextThirdTop
        });
        return text;
    }
    var createPopupFourthText = function() {
        var text = new fabric.Text('Workers + Slave daily food costs:', {
            fontSize: normalText,
            left: popupTextLeft,
            top: popupTextFourthTop
        });
        return text;
    }
    var createPopupFifthText = function() {
        var text = new fabric.Text('$10 x (2 + 5) = $ 100', {
            fontSize: normalText,
            left: popupTextMiddleLeft,
            top: popupTextFifthTop
        });
        return text;
    }
    var createPopupSixthText = function() {
        var text = new fabric.Text('Earnings from harvest: $10', {
            fontSize: normalText,
            left: popupTextLeft,
            top: popupTextSixthTop
        });
        return text;
    }
    var createPopupSeventhText = function() {
        var text = new fabric.Text('Daily net income: -$305', {
            fontSize: normalText,
            left: popupTextLeft,
            top: popupTextSeventhTop
        });
        return text;
    }

    var createEndGameTitleText = function() {
        var text = new fabric.Text('Game Over', {
            fontSize: bigText,
            left: popupTextMiddleLeft2,
            top: popupTextFirstTop
        });
        return text;
    }
    var createEndGameSecondText = function() {
        var text = new fabric.Text('Your daily costs put you into debt :(', {
            fontSize: normalText,
            left: popupTextSortaLeft,
            top: popupText15Top
        });
        return text;
    }
    var createEndGameThirdText = function() {
        var text = new fabric.Text('Cash: $ -164', {
            fontSize: normalText,
            left: popupTextMiddleLeft2,
            top: popupTextSecondTop
        });
        return text;
    }
    var createEndGameFourthText = function() {
        var text = new fabric.Text('Score: 15403', {
            fontSize: normalText,
            left: popupTextMiddleLeft2,
            top: popupTextThirdTop
        });
        return text;
    }
    var createEndGameFourAndHalfText = function() {
        var text = new fabric.Text('Day: 4', {
            fontSize: normalText,
            left: popupTextMiddleLeft2,
            top: popupTextFourthTop
        });
        return text;
    }
    var createEndGameFifthText = function() {
        var text = new fabric.Text('Workers: 2', {
            fontSize: normalText,
            left: popupTextMiddleLeft2,
            top: popupTextFifthTop
        });
        return text;
    }
    var createEndGameSixthText = function() {
        var text = new fabric.Text('Slaves: 5', {
            fontSize: normalText,
            left: popupTextMiddleLeft2,
            top: popupTextSixthTop
        });
        return text;
    }
    var createEndGameSeventhText = function() {
        var text = new fabric.Text('Fields: 5', {
            fontSize: normalText,
            left: popupTextMiddleLeft2,
            top: popupText65Top
        });
        return text;
    }
    var createEndGameEighthText = function() {
        var text = new fabric.Text('Close this popup to play again!', {
            fontSize: normalText,
            left: popupTextSortaLeft2,
            top: popupTextSeventhTop
        });
        return text;
    }
