/**
 * Created by Luke on 3/26/2016.
 */
var dailyLivingCost = 50;
var dailyWorkerWages = 40;
var dailyFoodCost = 10;

var processDailyStatus = function(numOfWorkers, numOfSlaves)
{
    var dailyLivingCostText = "Personal daily costs: $" + dailyLivingCost;
    var dailyWorkerWagesText = "Workers daily wages: $" + dailyWorkerWages
        + " x " + numOfWorkers + " = $" + dailyWorkerWages* numOfWorkers;
    var dailyFoodCostText = "Workers + Slaves daily food cost: ";

    var dailyFoodCost2Text = "$" + dailyFoodCost + " x ("
        + numOfWorkers + " + " + numOfSlaves + ") = $"
        + dailyFoodCost*(numOfWorkers+ numOfSlaves);
    var dailyEarningsText = "Money from food harvested: $" + moneyPaidToday;

    var dailyTotalCost = "Daily net income: $" + calculateDailyLivingTax(numOfWorkers, numOfSlaves);

    popupDailyUpdateText2.text = dailyLivingCostText;
    popupDailyUpdateText3.text = dailyWorkerWagesText;
    popupDailyUpdateText4.text = dailyFoodCostText;
    popupDailyUpdateText5.text = dailyFoodCost2Text;
    popupDailyUpdateText6.text = dailyEarningsText;
    popupDailyUpdateText7.text = dailyTotalCost;

}
var calculateDailyLivingTax = function (numOfWorkers, numOfSlaves) {
    return moneyPaidToday - (
            dailyLivingCost +
            dailyWorkerWages* numOfWorkers +
            dailyFoodCost*(numOfWorkers+ numOfSlaves)
        )
};