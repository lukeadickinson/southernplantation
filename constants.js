/**
 * Created by Luke on 3/25/2016.
 */

var canvasWidth = 900;
var canvasHeight = 800;
var spacing = 25;

var bigText = 36;
var normalText = 28;
var littleText = 24;
var tinyText = 18;


var fieldWidth = 175;
var fieldHeight = 200;

var barHeight = 100;
var statusBarWidth = ((fieldWidth +spacing) * 3) + spacing;

var barTopTextHeight = 5;
var barMiddleTextHeight = 38;
var barBottomTextHeight = 70;

var barBigTopTextHeight = 10;
var barBigBottomTextHeight = 50;

var barFirstTextLeft = 10;
var barSecondTextLeft = fieldWidth +spacing;

var fieldStatusWidth =fieldWidth +(spacing * 4);
var fieldStatusLeft = statusBarWidth - fieldStatusWidth;

var fieldStatusFirstTextLeft = 10 + fieldStatusLeft;
var fieldStatusSecondTextLeft = spacing * 6 + fieldStatusFirstTextLeft;
var fieldStatusThirdTextLeft = spacing * 3  + fieldStatusSecondTextLeft;

var sellButtonWidth = 100;
var sellButtonHeight = 30;

var personBackgroundTop = barHeight +  sellButtonHeight;
var personFirstTextTop = personBackgroundTop + 5;
var person2TextTop = personFirstTextTop + 20;
var person3TextTop = person2TextTop + 20;
var person4TextTop = person3TextTop + 20;

var personHeight = 100;


var personImageLeft = statusBarWidth + 5;
var personFirstTextLeft = statusBarWidth + 50;
var person2TextLeft = personFirstTextLeft + 100;

var sidebarLeft = statusBarWidth;
var sidebarWidth = canvasWidth - statusBarWidth
var slaveStatusTop = barHeight;

var popupLeft = 150;
var popupTop = 150;
var popupWidth = canvasWidth - (popupLeft *2);
var popupHeight = canvasHeight - (popupTop *2);

var doneHeight = 75;
var endDayTop = canvasHeight - doneHeight;
var endDayWidth = canvasWidth - sidebarLeft;

var popupTextLeft = popupLeft + 30;
var popupTextSortaLeft = popupLeft + 100;
var popupTextSortaLeft2= popupLeft + 120;

var popupTextMiddleLeft = popupLeft + popupWidth/3;
var popupTextMiddleLeft2 = popupLeft + popupWidth/3 + 25;
var popupTextRight = popupLeft + popupWidth/2 + 100;

var popupTextFirstTop = popupTop + 30;
var popupText15Top = popupTextFirstTop + 50;
var popupTextSecondTop = popupTextFirstTop + 100;
var popupTextThirdTop = popupTextSecondTop + 50;
var popupTextFourthTop = popupTextThirdTop + 50;
var popupTextFifthTop = popupTextFourthTop + 50;
var popupTextSixthTop = popupTextFifthTop + 50;
var popupText65Top = popupTextSixthTop + 50;

var popupTextSeventhTop = popupTextSixthTop + 100;

var slaveShipLeft = function(col)
{
    if(col == 0)
        return popupTextLeft;
    if(col == 1)
        return popupTextMiddleLeft2;
    if(col == 2)
        return popupTextRight;
}
