/**
 * Created by Luke on 3/26/2016.
 */
var weatherStates = {
    sunny: "sunny",
    raining : "raining",
    snowing: "snowing",
    veryHot: "very hot"
};

var generateCurrentWeather = function () {
    var randomNumber = getRandom(0,10);

    if(randomNumber > 4) {
        return weatherStates.sunny;
    }
    if(randomNumber > 6) {
        return weatherStates.raining;
    }
    if(randomNumber > 8) {
        return weatherStates.snowing;
    }

    return weatherStates.veryHot;
};