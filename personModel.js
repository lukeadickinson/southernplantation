/**
 * Created by Luke on 3/29/2016.
 */

var genders = {
    male: "male",
    female : "female"
};
var personTypes = {
    slave: 0,
    worker: 1,
    none: 2
}
var workerStatus = {
    resting: "resting",
    working: "working",
}

var initPeople = function()
{
    for(var row=0;row<6;row++) {
        people[row] = new Person(row);
        people[row].changePersonType();
    }
}
var workWorker = function (personNumber)
{
    var p = people[personNumber];

    if(p.personType == personTypes.none)
    {
        return;
    }
    if(fieldSelected == null)
    {
        return;
    }

    workerLoop(p.row, fieldSelected.row,fieldSelected.col);
}

var workerLoop = function(personNumber, row, col) {
    if(dayEnded == false) {
        if (fields[row][col].state == fieldStates.NotOwned) {
            return;
        }

        var shouldContinue = people[personNumber].work(1);
        var moneyPaid = fields[row][col].workField();
        payMoney(moneyPaid);

        if (shouldContinue) {
            setTimeout(function () {
                workerLoop(personNumber, row, col);
            }, 300);
        }
    }
}


var sellWorker = function (personNumber)
{
    var p = people[personNumber];

    if(p.personType == personTypes.none)
    {
        return;
    }

    var sellPrice = p.sellPrice();

    money+= sellPrice;
    updateMoney(money);

    p.personType = personTypes.none;
    p.changePersonType();

}
var buyWorker = function ()
{
    var openSlot = findOpenSlot();
    if(openSlot == -1)
    {
        return false;
    }
    var p = people[openSlot];

    p.personType = personTypes.worker;
    p.name = generateWorkerName();
    p.age = generateWorkerAge();
    p.gender = generateWorkerGender();
    p.buyPrice = workerCost();

    p.changePersonType();
    return true;
}
var buySlave = function (slaveNumber) {

    if (currentSlaves[slaveNumber].notForSale == true) {
        return false;
    }
    var openSlot = findOpenSlot();
    if(openSlot == -1)
    {
        return false;
    }
    var p = people[openSlot];

    currentSlaves[slaveNumber].notForSale = true;
    var slave = currentSlaves[slaveNumber];

    p.personType = personTypes.slave;
    p.name = slave.name;
    p.age = slave.age;
    p.gender = slave.gender;
    p.buyPrice = slave.buyPrice;

    money -= slave.buyPrice;
    updateMoney(money);
    boughtSlaves.push(p);
    return true;
}
var generateWorkerName = function()
{
    var randomNumber = parseInt(getRandom(0,5));
    var randomNumber2 = parseInt(getRandom(0,5));

    var firstname ="John";
    var lastname = "Smith";
    switch (randomNumber)
    {
        case 0:
            firstname = "John";
            break;
        case 1:
            firstname = "Frank"
            break;

        case 2:
            firstname = "Paul"
            break;

        case 3:
            firstname = "Henry"
            break;

        case 4:
            firstname = "Andy"
            break;

    }
    switch (randomNumber2)
    {
        case 0:
            lastname = "Smith";
            break;

        case 1:
            lastname = "Mill"
            break;

        case 2:
            lastname = "Brown"
            break;

        case 3:
            lastname = "Jones"
            break;

        case 4:
            lastname = "Green"
            break;

    }
    return firstname + " " + lastname;

}
var generateWorkerAge = function()
{
    var randomNumber = parseInt(getRandom(0,25));
    return 18 + randomNumber;
}
var generateWorkerGender = function()
{
    return genders.male;
}
var workerCost = function()
{
    return 0;
}

var createSlave = function ()
{
    var p = new Person(-1);

    p.personType = personTypes.worker;
    p.gender = generateSlaveGender();

    p.name = generateSlaveName(p.gender);
    p.age = generateSlaveAge();
    p.buyPrice = SlaveCost(p.age, p.gender);
    return p;
}

var generateSlaveName = function(gender)
{
    var randomNumber = parseInt(getRandom(0,6));
    var randomNumber2 = parseInt(getRandom(0,9));

    var firstname ="John";
    if(gender == genders.male) {
        switch (randomNumber) {
            case 0:
                firstname = "George";
                break;
            case 1:
                firstname = "Tom"
                break;

            case 2:
                firstname = "Hector"
                break;

            case 3:
                firstname = "Corslina"
                break;

            case 4:
                firstname = "Sippis"
                break;
            case 5:
                firstname = "Cudjo"
                break;

        }
    }
    if(gender == genders.female) {
        switch (randomNumber2) {
            case 0:
                firstname = "Minta";
                break;
            case 1:
                firstname = "Agnes"
                break;

            case 2:
                firstname = "Jinney Hay"
                break;

            case 3:
                firstname = "Juno"
                break;

            case 4:
                firstname = "Dafney"
                break;
            case 5:
                firstname = "Silva"
                break;
            case 6:
                firstname = "Sallie"
                break;
            case 7:
                firstname = "Grace"
                break;
            case 8:
                firstname = "Polly"
                break;
        }
    }
    return firstname ;
}
var generateSlaveAge = function()
{
    var randomNumber = parseInt(getRandom(0,40));
    return 8 + randomNumber;
}
var generateSlaveGender = function()
{
    var randomNumber = parseInt(getRandom(0,3));
    if(randomNumber <2)
        return genders.male;
    return genders.female;
}
var SlaveCost = function(age, gender)
{
    var cost = 0;
    if(age < 12)
    {
        cost += 40;
    }
    else if(age >= 12 && age < 17)
    {
        cost += 45;

    }
    else if(age >= 17 && age < 30)
    {
        cost += 70;

    }
    else if(age >= 30)
    {
        cost += 50;
    }
    if(gender == genders.male)
    {
        cost += 50;
    }
    else if(gender == genders.male)
    {
        cost += 30;

    }
    return cost;
}

var findOpenSlot = function ()
{
    for(var personNumber=0;personNumber<6;personNumber++) {
        if (people[personNumber].personType == personTypes.none)
        {
            return personNumber;
        }
    }
    return -1;
}

function Person (row) {

    this.row= row;

    this.notForSale = false;

    this.name = "John Cena";
    this.age = 25;
    this.gender = genders.male;
    this.personType = personTypes.none;
    //worker efficenty method

    this.percentageOfEnergy = 100;
    this.status = workerStatus.resting;
    this.health = "regular"

    this.buyPrice = parseInt(100);

    this.sellButtonShown = false;

    this.sellPrice = function()
    {
        return 0.8 * this.buyPrice;
    }
    this.changePersonType = function()
    {
        if(this.personType == personTypes.none)
        {
            canvas.remove(workerGroupArray[row]);
            canvas.remove(workerSellGroupArray[row]);
            canvas.remove(workerWorkGroupArray[row]);
            this.sellButtonShown = false;
            return;
        }

        canvas.add(workerGroupArray[row]);
        canvas.add(workerWorkGroupArray[row]);
        if(this.percentageOfEnergy == 100){
            canvas.add(workerSellGroupArray[row])
            this.sellButtonShown = true;
        }

        this.updatePerson();
    }
    this.updatePerson = function()
    {
        if(this.personType == personTypes.none){
            return;
        }

        var nameText = this.name;
        var ageGenderText = "age " + this.age +", " + this.gender;
        var healthText = "health: " +this.health;
        var statusText = "status: " +this.status;
        var energyText = "energy: " + this.percentageOfEnergy + "%";

        var sellText = "Lay off";

        var personColor = "rgba(253,230,151,1)";

        if(this.personType == personTypes.slave)
        {
            sellText = "Sell $" + this.sellPrice();
            personColor = "brown";
        }

        if(this.percentageOfEnergy == 100){
            if(this.sellButtonShown == false)
            {
                canvas.add(workerSellGroupArray[row])
                this.sellButtonShown = true;
            }
            energyText = "";
        }
        else
        {
            canvas.remove(workerSellGroupArray[row]);
            this.sellButtonShown = false;
        }

        workerNameTextArray[this.row].text = nameText;
        workerAgeGenderArray[this.row].text = ageGenderText;
        workerHealthArray[this.row].text = healthText;
        workerEnergyArray[this.row].text = energyText;
        workerActiveArray[this.row].text = statusText;
        workerSellTextArray[this.row].text = sellText;
        workerImageArray[this.row].fill = personColor;

    }
    this.work = function(workAmount)
    {
        if(this.percentageOfEnergy - workAmount <=0){
            this.percentageOfEnergy =0
            this.updatePerson();
            return false;
        }
        this.percentageOfEnergy -= workAmount;
        this.updatePerson();

        return true;
    }
    this.rest = function()
    {
        this.percentageOfEnergy = 100;
    }
}